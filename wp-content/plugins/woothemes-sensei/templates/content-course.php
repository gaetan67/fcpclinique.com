<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * Content-course.php template file
 *
 * responsible for content on archive like pages. Only shows the course excerpt.
 *
 * For single course content please see single-course.php
 *
 * @author 		Automattic
 * @package 	Sensei
 * @category    Templates
 * @version     1.9.0
 */

$cours = get_posts();

?>

<li <?php post_class(  WooThemes_Sensei_Course::get_course_loop_content_class() ); ?> >

    <?php  //echo Sensei_Utils::sensei_user_passed_course( get_the_ID(),get_current_user_id() ); 
    /**
     * This action runs before the sensei course content. It runs inside the sensei
     * content-course.php template.
     *
     * @since 1.9
     *
     * @param integer $course_id
     */
    do_action( 'sensei_course_content_before', get_the_ID() );
    ?>

    <section class="course-content">

        <section class="entry">

            <?php
            /**
             * Fires just before the course content in the content-course.php file.
             *
             * @since 1.9
             *
             * @param integer $course_id
             *
             * @hooked Sensei_Templates::the_title          - 5
             * @hooked Sensei()->course->course_image       - 10
             * @hooked  Sensei()->course->the_course_meta   - 20
             */
            //Sensei_Templates::the_title(); 
            
            //print_r($cours);

            echo '<h3 class="course-title"><a href="'. get_permalink() . '">'.  get_the_title() . ' - Publié le '. get_the_date('d F Y', get_the_ID()) . '</a></h3>';
            
            Sensei()->course->course_image()  ; ?>
            <div class="resume-objectif">
            <?php
            Sensei()->course->the_course_meta();
            //do_action('sensei_course_content_inside_before', get_the_ID() );
            ?>

            
                
            

                <?php echo get_field('objectif'); //the_excerpt(); ?>

            
                <a class="start-cours" href="<?php echo get_permalink(get_the_ID()) ?>" title="<?php echo __('Start','redbox') ?>"><?php echo __('Start','redbox') ?></a>
                <?php
                /**
                 * Fires just after the course content in the content-course.php file.
                 *
                 * @since 1.9
                 *
                 * @param integer $course_id
                 *
                 * @hooked  Sensei()->course->the_course_free_lesson_preview - 20
                 */
                do_action('sensei_course_content_inside_after', get_the_ID() );
                ?>
                
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>

        </section> <!-- section .entry -->

    </section> <!-- section .course-content -->

    <?php
    /**
     * Fires after the course block in the content-course.php file.
     *
     * @since 1.9
     *
     * @param integer $course_id
     *
     * @hooked  Sensei()->course->the_course_free_lesson_preview - 20
     */
    do_action('sensei_course_content_after', get_the_ID() );
    ?>


</li> <!-- article .(<?php esc_attr_e( join( ' ', get_post_class( array( 'course', 'post' ) ) ) ); ?>  -->