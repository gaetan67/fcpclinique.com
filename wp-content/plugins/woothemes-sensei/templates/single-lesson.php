<?php
/**
 * The Template for displaying all single lessons.
 *
 * Override this template by copying it to yourtheme/sensei/single-lesson.php
 *
 * @author 		Automattic
 * @package 	Sensei
 * @category    Templates
 * @version     1.9.0
 */
?>

<?php  get_sensei_header();  ?>

<?php the_post(); ?>

<article <?php post_class( array( 'lesson', 'post' ) ); ?>>

    <?php

        /**
         * Hook inside the single lesson above the content
         *
         * @since 1.9.0
         *
         * @param integer $lesson_id
         *
         * @hooked deprecated_lesson_image_hook - 10
         * @hooked deprecate_sensei_lesson_single_title - 15
         * @hooked Sensei_Lesson::lesson_image() -  17
         * @hooked deprecate_lesson_single_main_content_hook - 20
         */
        do_action( 'sensei_single_lesson_content_inside_before', get_the_ID() );

    ?>

    <section class="entry fix">

        <?php

        if ( sensei_can_user_view_lesson() ) { ?>

            
        
            <div class="lescommandites">
                <?php if( have_rows('commandites') ): ?>

                    <div class="commandites">
                        <h3>Commanditaires</h3>

                    <?php while( have_rows('commandites') ): the_row(); 

                        // vars
                        $image = get_sub_field('logo_du_commandite');
                        $nom = get_sub_field('nom_du_commandite');
                        $link = get_sub_field('lien_du_commandite');

                        ?>
                        <?php if( $link ): ?>
                            <a class="commandite" title="<?php echo $nom; ?>" href="" target="_blank">
                        <?php else: ?>
                            <span class="commandite">
                        <?php endif; ?>
                                <img src="<?php echo $image['url']; ?>" title="<?php echo $nom ?>" alt="<?php echo $nom ?>" />
                        
                        <?php if( $link ): ?>
                            </a>
                        <?php else: ?>
                            </span>
                        <?php endif; ?>

                        

                        

                    <?php endwhile; ?>

                    </div>

                <?php endif; ?>
            </div>

            <?php    if( apply_filters( 'sensei_video_position', 'top', $post->ID ) == 'top' ) {

                do_action( 'sensei_lesson_video', $post->ID );

            } ?>
            <?php
            $document = get_field('document_de_formation');
             if($document){ ?>
                <div class="document">
                    <a target="_blank" href="<?php echo $document['url'];  ?>" target="_blank"  title='Télécharger PDF'>Télécharger PDF</a>
                </div>
            <?php  } ?>
                
                
                    <!--<a class="commandite" href="" target="_blank">
                        
                    </a>
                    <div class="commandite">
                        
                    </div>
                    <div class="commandite">
                        
                    </div>-->
                    
                

            
            <div style="clear: both;"></div>
            
            <?php   the_content(); ?>

    <?php } else {
            ?>

                <p> <?php the_excerpt(); ?> </p>

            <?php
        }

        ?>

    </section>

    <?php

        /**
         * Hook inside the single lesson template after the content
         *
         * @since 1.9.0
         *
         * @param integer $lesson_id
         *
         * @hooked Sensei()->frontend->sensei_breadcrumb   - 30
         */
        do_action( 'sensei_single_lesson_content_inside_after', get_the_ID() );

    ?>

</article><!-- .post -->

<?php get_sensei_footer(); ?>