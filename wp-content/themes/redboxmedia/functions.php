<?php

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
}

/* creating a shortcode */
/*
function foobar_func( $atts ){
	return "foo and bar";
}
add_shortcode( 'foobar', 'foobar_func' );
*/


// Add Your Menu Locations
function register_my_menus() {
  register_nav_menus(
    array(  
    	'footer_navigation' => __( 'Footer Navigation' )
    )
  );
} 
add_action( 'init', 'register_my_menus' );




// custom my account page

function my_custom_my_account_menu_items( $items ) {
    $items = array(
        
        'customer-logout'   => __( 'Logout', 'woocommerce' ),
        'edit-account'      => __( 'Détails du compte', 'woocommerce' ),
        'orders'            => __( 'Historique', 'woocommerce' ),
        //'downloads'       => __( 'Downloads', 'woocommerce' ),
        
        //'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
        
        'edit-address'    => __( 'Adresses', 'woocommerce' )
        
        
    );

    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );

// move comment textarea to bottom
function wpb_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


// show only bundle products
/*add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );
function custom_pre_get_posts_query( $q )
{

if (!$q->is_main_query() || !is_shop()) return;
    if ( ! is_admin() )
    {
        $q->set( 'tax_query', array(array(
            'taxonomy' => 'product_type',
            'field' => 'slug',
            'terms' => 'bundle'
        )));

    }
}*/
// resize images
add_image_size( 'avatar', 104, 104, true );
add_image_size( 'partenaires_logo', 225, 95, false );



// placeholder billing form
// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_first_name']['placeholder'] = 'Prénom*';
     $fields['billing']['billing_last_name']['placeholder'] = 'Nom*';
     $fields['billing']['billing_company']['placeholder'] = "Nom de l'entreprise";
     $fields['billing']['billing_phone']['placeholder'] = 'Téléphone*';
     $fields['billing']['billing_city']['placeholder'] = 'Ville*';
     $fields['billing']['billing_state']['placeholder'] = 'Province*';
     $fields['billing']['billing_country']['placeholder'] = 'Pays*';
     $fields['billing']['billing_postcode']['placeholder'] = 'Code postal*';
     $fields['billing']['billing_email']['placeholder'] = 'Courriel*';
     $fields['billing']['billing_address_1']['placeholder'] = 'Adresse*';
     $fields['billing']['billing_address_2']['placeholder'] = 'Appartement, bureau, etc. (optionnel)';
     
     return $fields;
}

// Reorder Checkout Fields
/*add_filter('woocommerce_checkout_fields','reorder_woo_fields');
 
function reorder_woo_fields($fields) {
    
    $fields2['billing']['billing_first_name'] = $fields['billing']['billing_first_name'];
    $fields2['billing']['billing_last_name'] = $fields['billing']['billing_last_name'];
    $fields2['billing']['billing_company'] = $fields['billing']['billing_company'];
    $fields2['billing']['billing_email'] = $fields['billing']['billing_email'];
    $fields2['billing']['billing_phone'] = $fields['billing']['billing_phone'];
    $fields2['billing']['billing_address_1'] = $fields['billing']['billing_address_1'];
    $fields2['billing']['billing_address_2'] = $fields['billing']['billing_address_2'];
    
    $fields2['billing']['billing_state'] = $fields['billing']['billing_state'];
    $fields2['billing']['billing_city'] = array(
        'placeholder' => 'Ville*',
        'required'  => true,
        'class'     => array('form-row-last'),
        'clear'     => true,
    );
    $fields2['billing']['billing_country'] = array(
        'required'  => true,
        'class'     => array('form-row-first'),
        'clear'     => false
    );
    $fields2['billing']['billing_postcode'] = $fields['billing']['billing_postcode']; 


    // Add full width Classes and Clears to Adjustments
    
    

    return $fields2;
}*/

//Change the order of fields on edit my address page
/*add_filter("woocommerce_default_address_fields", "order_address_fields");

function order_address_fields($fields) {

    $order = array(
        "first_name",
        "last_name",
        "email",
        "company",
        "country",
        "address_1",
        "city",
        "postcode",
    );
    foreach($order as $field)
    {
        $ordered_fields[$field] = $fields[$field];
    }

    $fields = $ordered_fields;
    return $fields;
}*/


add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

// Our hooked in function - $address_fields is passed via the filter!
function custom_override_default_address_fields( $address_fields ) {

     $address_fields['first_name']['placeholder'] = 'Prénom*';
     //$address_fields['first_name']['required'] = false;

    $address_fields['last_name']['placeholder'] = 'Nom*';
     //$address_fields['last_name']['required'] = false;

    $address_fields['company']['placeholder'] = "Nom de l'entreprise";

    $address_fields['address_1']['placeholder'] = "Adresse*";

    $address_fields['address_2']['placeholder'] = "Appartement, bureau, etc. (optionnel)";

    $address_fields['city']['placeholder'] = "Ville*";

    $address_fields['postcode']['placeholder'] = "Code postal*";

    $address_fields['country']['placeholder'] = "Pays*";

    $address_fields['state']['placeholder'] = "Province*";

    $address_fields['email'] = array(

        'label'     => __('Email Address', 'woocommerce'),

        'placeholder'   => _x('Courriel*', 'placeholder', 'woocommerce'),

        'required'  => true,

        'class'     => array('form-row-first'),

        

    );

     return $address_fields;
}


add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );



function my_show_extra_profile_fields( $user ) { ?>

    <h3>Extra profile information</h3>

    <table class="form-table">
        <tr>
            <th><label for="student">Si étudiant ou pharmacien</label></th>

            <td>
                
                <input type="radio" name="student" id="student" value="etudiant" <?php if( get_the_author_meta( 'student', $user->ID ) == 'etudiant') echo ' checked'; ?> class="" /><label for="student">Étudiant en pharmacie</label><br/>
                <input type="radio" name="student" id="pharmacien" value="pharmacien" <?php if( get_the_author_meta( 'student', $user->ID ) == 'pharmacien') echo ' checked'; ?> class="" /><label for="pharmacien">Pharmacien</label><br/>
               
            </td>
        </tr>
        <tr>
            <th><label for="pratique">Province</label></th>

            <td>
                <?php 
                    $listProvince = array(
                        'Alberta' => "Alberta",
                        "Colombie-Britannique" => "Colombie-Britannique",
                        "Ile-du-Prince-Edouard" => "Ile-du-Prince-Édouard",
                        "Manitoba" => "Manitoba",
                        "Nouveau-Brunswick" => "Nouveau-Brunswick",
                        "Nouvelle-Ecosse" => "Nouvelle-Écosse",
                        "Ontario" => "Ontario",
                        "Quebec" => "Québec",
                        "Saskatchewan" => "Saskatchewan",
                        "Terre-Neuve-et-Labrador" => "Terre-Neuve-et-Labrador",
                        "Nunavut" => "Nunavut",
                        "Territoires du Nord-Ouest" => "Territoires du Nord-Ouest",
                        "Yukon" => "Yukon",
                        "Autre" => "Autre"
                    );
            

        ?>
        
        <select id="user_province" name="user_province" >
                <option value="0">Choix de province</option>
            <?php foreach($listProvince as $uneProvince => $provinceValue){ ?>
                <option value="<?php echo $uneProvince ?>" <?php if($uneProvince === get_the_author_meta( 'user_province', $user->ID ) ) {echo ' selected';}?>><?php echo $provinceValue ?></option>
            <?php }?>
            
            
        </select><br/>
                
                <span class="description">Entrer le milieu de pratique</span>
            </td>
        </tr>

        <tr>
            <th><label for="licence">Licence</label></th>

            <td>
                <input type="text" name="licence" id="licence" value="<?php echo esc_attr( get_the_author_meta( 'licence', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">SVP entrer votre numéro de licence</span>
            </td>
        </tr>
        <tr>
            <th><label for="pratique">Milieu de pratique</label></th>

            <td>
                <?php 
            $listPratique = array(
                'pharmacie communautaire' => "pharmacie communautaire",
                "pharmacie hopital" => "pharmacie d’hôpital",
                "pharmacien independant" => "pharmacien indépendant",
                "universite" => "université",
                "autre" => "autre"
            );
            //echo $pratique;
            //echo get_the_author_meta( 'pratique', $user->ID );

        ?>
        <!--<label for="pratique"></label>-->
        
        <select id="pratique" name="pratique" >
                <option value="0">Choix de pratique</option>
            <?php foreach($listPratique as $unePratique => $pratiqueValue){ ?>
                <option value="<?php echo $unePratique ?>" <?php if($unePratique === get_the_author_meta( 'pratique', $user->ID ) ) {echo ' selected';}?>><?php echo $pratiqueValue ?></option>
            <?php }?>
            
            
        </select><br/>
                
                <span class="description">Entrer le milieu de pratique</span>
            </td>
        </tr>

    </table>
<?php }



 
/**
 * This is to save user input into database
 * hook: woocommerce_save_account_details
 */
add_action( 'woocommerce_save_account_details', 'my_woocommerce_save_account_details' );
 
function my_woocommerce_save_account_details( $user_id ) {
    update_user_meta( $user_id, 'licence', htmlentities( $_POST[ 'licence' ] ) ); 
    update_user_meta( $user_id, 'pratique', htmlentities( $_POST[ 'pratique' ] ) ); 
    update_user_meta( $user_id, 'student', htmlentities( $_POST[ 'student' ] ) ); 
    update_user_meta( $user_id, 'user_province', htmlentities( $_POST[ 'user_province' ] ) ); 
} // end func

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

    update_user_meta( $user_id, 'licence', htmlentities( $_POST[ 'licence' ] ) ); 
    update_user_meta( $user_id, 'pratique', htmlentities( $_POST[ 'pratique' ] ) ); 
    update_user_meta( $user_id, 'student', htmlentities( $_POST[ 'student' ] ) ); 
    update_user_meta( $user_id, 'user_province', htmlentities( $_POST[ 'user_province' ] ) );
}


//1. Add a new form element...
/*add_action( 'register_form', 'myplugin_register_form' );
function myplugin_register_form() {

    $first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
        
        ?>
        <p>
            <label for="first_name"><?php _e( 'First Name', 'mydomain' ) ?><br />
                <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" /></label>
        </p>
        <?php
    }*/
    

    //2. Add validation. In this case, we make sure first_name is required.
   // add_filter( 'registration_errors', 'myplugin_registration_errors', 10, 3 );
    function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) {
        //if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
        $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
        if ( $_POST['first_name'] == ''  ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: You must include a first name.', 'mydomain' ) );
        }
        if ( empty( $_POST['last_name'] ) || ! empty( $_POST['last_name'] ) && trim( $_POST['last_name'] ) == '' ) {
            $errors->add( 'last_name_error', __( '<strong>ERROR</strong>: You must include a last name.', 'mydomain' ) );
        }
        if ( empty( $_POST['licence'] ) || ! empty( $_POST['licence'] ) && trim( $_POST['licence'] ) == '' ) {
            $errors->add( 'licence_error', __( '<strong>ERROR</strong>: You must include a licence.', 'mydomain' ) );
        }
        if ( empty( $_POST['student'] ) || ! empty( $_POST['student'] ) && trim( $_POST['student'] ) == '' ) {
            $errors->add( 'licence_error', __( '<strong>ERROR</strong>: You must include a student.', 'mydomain' ) );
        }
        //if ( empty( $_POST['pratique'] ) || ! empty( $_POST['pratique'] ) && trim( $_POST['pratique'] ) == '' ) {
        if ( $_POST['pratique']  == '0' ) {
            $errors->add( 'pratique_error', __( '<strong>ERROR</strong>: You must include pratique.', 'mydomain' ) );
        }

        return $errors;
    }

    //3. Finally, save our extra registration user meta.
    add_action( 'user_register', 'myplugin_user_register' );
    function myplugin_user_register( $user_id ) {
        if ( ! empty( $_POST['first_name'] ) ) {
            update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
        }
        if ( ! empty( $_POST['last_name'] ) ) {
            update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
        }
        if ( ! empty( $_POST['licence'] ) ) {
            update_user_meta( $user_id, 'licence', trim( $_POST['licence'] ) );
        }
        if ( ! empty( $_POST['pratique'] ) ) {
            update_user_meta( $user_id, 'pratique', trim( $_POST['pratique'] ) );
        }
        if ( ! empty( $_POST['student'] ) ) {
            update_user_meta( $user_id, 'student', trim( $_POST['student'] ) );
        }
        if ( ! empty( $_POST['user_province'] ) ) {
            update_user_meta( $user_id, 'user_province', trim( $_POST['user_province'] ) );
        }
    }

/**
 * New user registrations should have display_name set to 'firstname lastname'
 * Best used on 'user_register'
 * @param int $user_id The user ID
 * @return void
 * @uses get_userdata()
 * @uses wp_update_user()
 */
function set_default_display_name( $user_id ) {
  $user = get_userdata( $user_id );
  $name = sprintf( '%s %s', $user->first_name, $user->last_name );
  $args = array(
    'ID' => $user_id,
    'display_name' => $name,
    'nickname' => $name
  );
  wp_update_user( $args );
}
add_action( 'user_register', 'set_default_display_name' );


add_filter('woocommerce_add_to_cart_validation','rei_woocommerce_add_to_cart_validation',20, 2);
function rei_woocommerce_add_to_cart_validation($valid, $product_id){
    $current_user = wp_get_current_user();
    if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product_id)) {
        wc_add_notice( __( "You've already purchased this module! It can only be purchased once.", 'redbox' ), 'error' );
        $valid = false;
    }
    return $valid;
}

/**
 * Auto Complete all WooCommerce orders.
 */
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    $order->update_status( 'completed' );
}

/*function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'yes':'no';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');*/



?>
