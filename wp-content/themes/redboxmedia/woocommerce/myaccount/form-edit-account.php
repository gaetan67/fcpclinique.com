<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_edit_account_form' ); ?>
<?php 
	$user_id = get_current_user_id();
    
    $licence = get_user_meta( $user_id, 'licence', true );
    $pratique = get_user_meta( $user_id, 'pratique', true );
    $student = get_user_meta( $user_id, 'student', true );
    $user_province = get_user_meta( $user_id, 'user_province', true );
?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
	<?php //print_r($user); ?>
	<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide if-student">
		<input type="radio" name="student" id="student" value="etudiant" <?php if($student == 'etudiant') echo ' checked'; ?>/> <label for="student">Étudiant en pharmacie</label>
		<input type="radio" name="student" id="pharmacien" <?php if($student == 'pharmacien') echo ' checked'; ?> value="pharmacien" /> <label class="pharmacien" for="pharmacien">Pharmacien</label>
	</p>
	<p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first" style="position: relative;">
		<span style="position: absolute;top:0;left:0; right: 0;bottom: 0; display: block; height: 56px;"></span>
		<!--<label for="account_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text"  name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" placeholder="<?php _e( 'First name', 'woocommerce' ); ?>" />
		
	</p>
	<p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last" style="position: relative;">
		<span style="position: absolute;top:0;left:0; right: 0;bottom: 0; display: block; height: 56px;"></span>
		<!--<label for="account_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text"  name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" placeholder="<?php _e( 'Last name', 'woocommerce' ); ?>" />
	</p>
	<div class="clear"></div>

	<p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first">
		<!--<label for="account_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>-->
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" placeholder="<?php _e( 'Email address', 'woocommerce' ); ?>" />
	</p>
	<p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last " id="province_field" >
		<?php
            $listProvince = array(
                'Alberta' => "Alberta",
                "Colombie-Britannique" => "Colombie-Britannique",
                "Ile-du-Prince-Edouard" => "Ile-du-Prince-Édouard",
                "Manitoba" => "Manitoba",
                "Nouveau-Brunswick" => "Nouveau-Brunswick",
                "Nouvelle-Ecosse" => "Nouvelle-Écosse",
                "Ontario" => "Ontario",
                "Quebec" => "Québec",
                "Saskatchewan" => "Saskatchewan",
                "Terre-Neuve-et-Labrador" => "Terre-Neuve-et-Labrador",
                "Nunavut" => "Nunavut",
                "Territoires du Nord-Ouest" => "Territoires du Nord-Ouest",
                "Yukon" => "Yukon",
                "Autre" => "Autre"
            );
        ?>
        <span class="select-style">
	        <select id="user_province" name="user_province" title="SVP sélectionner votre province">
	                <option value="">Votre province*</option>
	            <?php foreach($listProvince as $uneProvince => $provinceValue){ ?>
	                <option value="<?php echo $uneProvince ?>" <?php if(esc_attr($uneProvince) === esc_attr($user_province) ) {echo ' selected';}?> ><?php echo $provinceValue ?></option>
	            <?php }?>
	            
	            
	        </select>
		</span>
        
	</p>
	
	<div class="clear"></div>

	<p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first not_student" id="practice_field" >
		<?php
            $listPratique = array(
                'pharmacie communautaire' => "pharmacie communautaire",
                "pharmacie hopital" => "pharmacie d’hôpital",
                "pharmacien independant" => "pharmacien indépendant",
                "universite" => "université",
                "autre" => "autre"
            );
            //echo $pratique;

        ?>
        <span class="select-style">
        
	        <select id="pratique" name="pratique"  tabindex="-1" title="Choix de pratique"   >
	                <option value="0">Milieu de pratique</option>
	            <?php foreach($listPratique as $unePratique => $pratiqueValue){ ?>
	                <option value="<?php echo $unePratique ?>" <?php if(esc_attr($unePratique) === esc_attr($pratique) ) {echo ' selected';}?>><?php echo $pratiqueValue ?></option>
	            <?php }?>
	            
	            
	        </select>
        </span>
	</p>
	<p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last not_student" style="position: relative;">
		<span style="position: absolute;top:0;left:0; right: 0;bottom: 0; display: block; height: 56px;"></span>
		<input type="text" id="licence" name="licence"  value="<?php echo esc_attr( $licence ); ?>" class="woocommerce-Input woocommerce-Input--licence input-text" placeholder="Licence" />
	</p>
	
	<div class="clear"></div>

	<fieldset>
		<h3><?php _e( 'Password Change', 'woocommerce' ); ?></h3>

		<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
			<!--<label for="password_current"><?php _e( 'Current Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" placeholder="<?php _e( 'Current Password (leave blank to leave unchanged)', 'woocommerce' ); ?>" />
		</p>
		<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
			<!--<label for="password_1"><?php _e( 'New Password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" placeholder="<?php _e( 'New Password (leave blank to leave unchanged)', 'woocommerce' ); ?>" />
		</p>
		<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
			<!--<label for="password_2"><?php _e( 'Confirm New Password', 'woocommerce' ); ?></label>-->
			<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2"  placeholder="<?php _e( 'Confirm New Password', 'woocommerce' ); ?>" />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details' ); ?>
		<input type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
