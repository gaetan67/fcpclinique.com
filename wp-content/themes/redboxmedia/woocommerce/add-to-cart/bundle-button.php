<?php
/**
 * Bundle add-to-cart button template.
 *
 * Override this template by copying it to 'yourtheme/woocommerce/single-product/add-to-cart/bundle-button.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 4.7.4
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if(is_user_logged_in()){ ?>
	<button type="submit" class="single_add_to_cart_button bundle_add_to_cart_button button alt">22<?php echo "M'inscrire"; //$product->single_add_to_cart_text(); ?></button>
<?php } else { ?>
	<a href="<?php echo get_permalink(372); ?>" title="S'abonner"  class="single_add_to_cart_button bundle_add_to_cart_button button alt">33<?php echo "S'abonner"; //$product->single_add_to_cart_text(); ?></a>	
<?php }
?>
