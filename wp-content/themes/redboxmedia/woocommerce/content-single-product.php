<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }

$options = get_nectar_theme_options(); 
$product_style = (!empty($options['product_style'])) ? $options['product_style'] : 'classic';

?>

<div itemscope data-project-style="<?php echo $product_style; ?>" itemtype="<?php echo woocommerce_get_product_schema(); ?>" data-tab-pos="<?php echo (!empty($options['product_tab_position'])) ? $options['product_tab_position'] : 'default'; ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			woocommerce_template_single_title();
			woocommerce_template_single_rating();
		
			
			the_content();
			echo "<span class='inscrip-title'>Frais d'inscription au module : </span>"; 
			woocommerce_template_single_price();
			echo '<span class="taxes"> + tx</span>';
			
			woocommerce_template_single_add_to_cart();
			
			
			
			woocommerce_template_single_sharing();

			//nectar_blog_social_sharing();
			?>
			

			
			<?php
			//do_action( 'woocommerce_single_product_summary' );
		?>

	
	</div><!-- .summary -->
</div>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>
	<?php //comments_template(); ?>
	
		
		<?php if( have_rows('cours') ): ?>

			<div class="all-lessons">
				<p class="title">Ce module inclut les cours ci-dessous:</p>
			<?php while( have_rows('cours') ): the_row(); 

				// vars
				$thumbnail = get_sub_field('thumbnail');
				$titre_du_cours = get_sub_field('titre_du_cours');
				$info_bref_du_cours = get_sub_field('info_bref_du_cours');
				$video_demo = get_sub_field('video_demo');

				?>

				
				<div class="bundled_product bundled_product_summary product " style="">
					<div class="bundled_product_images">
						<a href="<?php echo $thumbnail['url'] ?>" class="bundled_product_image zoom" title="<?php echo $titre_du_cours ?>" rel="prettyPhoto">
							<img width="333" height="187" src="<?php echo $thumbnail['sizes']['shop_thumbnail'] ?>" alt="<?php echo $thumbnail['alt'] ?>" title="<?php echo $thumbnail['alt'] ?>" >
						</a>
					</div>
					<div class="details">
						<h4 class="bundled_product_title product_title">
							<span class="bundled_product_title_inner"><?php echo $titre_du_cours ?></span>
						</h4>
						<div class="bundled_product_excerpt product_excerpt">
							<?php echo $info_bref_du_cours; ?>
						</div>
						<?php
						if ( is_user_logged_in() ) { ?>
						   <a class="btn_demo" rel="prettyPhoto" href="<?php echo $video_demo; ?>" >Démo</a>
						<?php } else { ?>
						    <a class="btn_demo" href="<?php echo get_permalink(372); ?>" >Démo</a>
						<?php }

						?>

						
						
					</div>
					<div style="clear: both;"></div>
				</div>

			<?php endwhile; ?>

			</div>

		<?php endif; ?>
		
		
	

	
	<?php if( have_rows('temoignages') ): ?>
		<div class="temoignages">
			<h3>Témoignages</h3>

			<?php while( have_rows('temoignages') ): the_row(); 

				// vars
				$nom = get_sub_field('nom');
				$titre__poste = get_sub_field('titre__poste');
				$photo = get_sub_field('photo');
				$temoignage = get_sub_field('temoignage');

				?>

				<div class="temoignage">
					<div class="avatar">
						<img src="<?php echo $photo['sizes']['avatar']; ?>" alt="<?php echo $photo['alt'] ?>" />
					</div>
					<p class="name"><?php echo $nom; ?></p>
					<?php if( $titre__poste ): ?>
						<p class="poste"><?php echo $titre__poste; ?></p>
					<?php endif; ?>
					
					<?php echo $temoignage; ?>

					

				</div>

			<?php endwhile; ?>
			<div style="clear: both;"></div>
		</div>

	<?php endif; ?>
		<!--<div class="temoignage">
			<div class="avatar">
				<img src="" />
			</div>
			<p class="name">Name</p>
			<p class="poste">Poste</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		</div>
		<div class="temoignage">
			<div class="avatar">
				<img src="" />
			</div>
			<p class="name">Name</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		</div>
		<div class="temoignage">
			<div class="avatar">
				<img src="" />
			</div>
			<p class="name">Name</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
		</div>-->
		
	<meta itemprop="url" content="<?php the_permalink(); ?>" />



</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
