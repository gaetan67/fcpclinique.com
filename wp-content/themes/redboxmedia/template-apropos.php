<?php 
/*template name: À propos*/
get_header(); ?>


<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content objectifs">
		
		<div class="row">
			<h2><?php echo get_field('titre_objectif'); ?></h2>
			<span class="underline"></span>
			<?php if( have_rows('objectif') ): $i = 1;?>
				<?php while( have_rows('objectif') ): the_row(); 

					// vars
					$photo = get_sub_field('icon');
					$texte = get_sub_field('texte');

					?>

					<div class="objectif <?php if($i == 4) echo ' last' ?>">
						
						<img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt'] ?>" />					
						
						<?php echo $texte; ?>									    
					</div>
				<?php $i++; endwhile; ?>

			<?php endif; ?>
			<!--<div class="objectif">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/objectif-1.png" alt="">
				<p>FCP Clinique réponds au besoin grandissant de formation continue du pharmacien, professionnel
de la santé, ayant un rôle central dans les soins de première ligne.</p>

			</div>
			<div class="objectif">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/objectif-2.png" alt="">
				<p>FCP Clinique réponds au besoin grandissant de formation continue du pharmacien, professionnel
de la santé, ayant un rôle central dans les soins de première ligne.</p>

			</div>
			<div class="objectif last">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/objectif-3.png" alt="">
				<p>FCP Clinique réponds au besoin grandissant de formation continue du pharmacien, professionnel
de la santé, ayant un rôle central dans les soins de première ligne.</p>

			</div>-->
			<div style="clear: both;"></div>


				
	
		</div><!--/row-->
		
	</div><!--/container-->
	<div class="banner banner-green">
		<div class="container main-content ">
		
			<div class="row">

				<h3><?php echo get_field('titre_banniere_centre'); ?></h3>
				<?php echo get_field('text_banniere_centre'); ?>
				<?php if(get_field('lien_bouton')){ ?>
					<a class="btn_link green" href="<?php echo get_field('lien_bouton'); ?>" title="<?php echo get_field('text_bouton'); ?>"><?php echo get_field('text_bouton'); ?></a>
				<?php }?>
			</div>
		</div>
	</div>
	<div class="container main-content vision">
		
		<div class="row">
			
			
			<h2><?php echo get_field('titre_vision'); ?></h2>
			<span class="underline"></span>
			<div class="text_2col">
				<?php echo get_field('texte_vision'); ?>

			</div>


				
	
		</div><!--/row-->
		
	</div><!--/container-->
	<div class="banner-image" style="background-image: url(<?php $image = get_field('image_arriere_plan'); echo $image['url']; ?>)">
		<div class="container main-content ">
		
			<div class="row">
				<?php echo get_field('texte_banniere_bas'); ?>
				<?php if(get_field('lien_bouton_banniere_bas')){ ?>
					<a class="btn_link_plus " href="<?php echo get_field('lien_bouton_banniere_bas'); ?>" title="En savoir +"><?php echo get_field('text_bouton_banniere_bas'); ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
	
</div>
<?php get_footer(); ?>