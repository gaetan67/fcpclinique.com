<?php 
/*template name: Accueil */
get_header(); ?>


<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	<div class="banner banner-blue">
		<div class="container main-content ">
		
			<div class="row">

				<h3><?php echo get_field('titre_bandebleu'); ?></h3>
				<?php echo get_field('texte_bandebleu'); ?>
				
			</div>
		</div>
	</div>
	<div class="wrap_white">
		<div class="container main-content modules">
			
			<div class="row">
				
				

				<h2><?php echo get_field('titre_modules'); ?></h2>
				<span class="underline"></span>
				<div class="wrap-modules">

					<?php if( have_rows('module') ): $i = 1;?>
						<?php while( have_rows('module') ): the_row(); 

							// vars
							$photo = get_sub_field('photo');
							$titre = get_sub_field('titre');
							$texte = get_sub_field('texte');
							$lien_bouton = get_sub_field('lien_bouton');
							$texte_bouton = get_sub_field('texte_bouton');

							?>

							<div class="one-module <?php if($i == 4) echo ' last' ?>">
								<div class="photo">
									<img src="<?php echo $photo['sizes']['shop_thumbnail']; ?>" alt="<?php echo $photo['alt'] ?>" />
								</div>
								<div class="texte">
									<h3><?php echo $titre; ?></h3>
									<?php echo $texte; ?>
									<?php if( $lien_bouton ): ?>
										<a class="savoir-plus" href="<?php echo $lien_bouton; ?>" title="<?php echo $texte_bouton; ?>">
											<?php echo $texte_bouton; ?>
										</a>
									<?php endif; ?>	
								</div>				    
							</div>
						<?php $i++; endwhile; ?>

					<?php endif; ?>
					
					<div style="clear: both;"></div>
				</div>


					
		
			</div><!--/row-->
			
		</div><!--/container-->
	</div>
	<div class="banner banner-green">
		<div class="container main-content ">
		
			<div class="row">

				<h3><?php echo get_field('titre_bandeverte'); ?></h3>
				<?php echo get_field('texte_bandeverte'); ?>
				
			</div>
		</div>
	</div>
	<div class="wrap_white">
		<div class="container main-content visions">
			
			<div class="row">
				
				
				<h2><?php echo get_field('titre_objectif'); ?></h2>
				<span class="underline"></span>
				<?php if( have_rows('etapes') ): 
					
						$etapes = get_field('etapes');

						?>

						

				<?php endif; ?>
				<div class="wrap-vision">
					<div class="vision etape1">
						<div class="content">
							<div class="top" style="background-image: url(<?php echo $etapes[0]['icon']['url']; ?>)">
								
							</div>
							<h3><?php echo $etapes[0]['etape']; ?></h3>
							<?php echo $etapes[0]['texte_etape']; ?>
					
						</div>
					</div>
					<div>
						<div class="vision etape2">
							<div class="content">
								<div class="top" style="background-image: url(<?php echo $etapes[1]['icon']['url']; ?>)">
								
							</div>
							<h3><?php echo $etapes[1]['etape']; ?></h3>
							<?php echo $etapes[1]['texte_etape']; ?>
						
							</div>
						</div>
						<div class="vision etape4">
							<div class="content">
								<div class="top" style="background-image: url(<?php echo $etapes[3]['icon']['url']; ?>)">
								
							</div>
							<h3><?php echo $etapes[3]['etape']; ?></h3>
							<?php echo $etapes[3]['texte_etape']; ?>
						
							</div>
						</div>
						<div class="vision inscription" style="background-image: url(<?php $image = get_field('image_inscrivez-vous'); echo $image['url'] ?>)">
							<a href="<?php echo get_permalink(75); ?>" title="Abonnez-vous">Abonnez-vous</a>
						

						</div>
						<div style="clear: both;"></div>
					</div>

					
					<div class="vision etape3">
						<div class="content">
							<div class="top" style="background-image: url(<?php echo $etapes[2]['icon']['url']; ?>)">
								
							</div>
							<h3><?php echo $etapes[2]['etape']; ?></h3>
							<?php echo $etapes[2]['texte_etape']; ?>
					
						</div>
					</div>
					<div class="vision etape4 mobile">
							<div class="content">
								<div class="top" style="background-image: url(<?php echo $etapes[3]['icon']['url']; ?>)">
								
							</div>
							<h3><?php echo $etapes[3]['etape']; ?></h3>
							<?php echo $etapes[3]['texte_etape']; ?>
						
							</div>
						</div>
					
					<div style="clear: both;"></div>
				</div>


					
		
			</div><!--/row-->
			
		</div><!--/container-->
	</div>
	<div class="banner banner-turkoise">
		<div class="container main-content ">
		
			<div class="row">
				<h3><?php echo get_field('titre_turkoise'); ?></h3>
				<?php echo get_field('texte_turkoise'); ?>

			</div>
		</div>
	</div>
	<div class="banner-image inscrivez-vous">
		<div class="container main-content ">
		
			<div class="row">
				<?php echo get_field('titre_bande_image'); ?>
				
				<a class="btn_link_turkoise" href="<?php echo get_field('lien_bouton_bande_image'); ?>" title="<?php echo get_field('texte_bouton_bande_image'); ?>"><?php echo get_field('texte_bouton_bande_image'); ?></a>
			</div>
		</div>
	</div>
	<div class="wrap_white">
		<div class="container main-content partenaires">
			
			<div class="row">
				
				
				<h2><?php echo get_field('titre_partenaires'); ?></h2>
				<span class="underline"></span>
				<?php if(get_field('texte_de_remerciement')){ ?>
					<p class="partenaires-remerciement"><?php echo get_field('texte_de_remerciement'); ?></p>
				<?php } ?>
				<div class="wrap-partenaire">
					<?php if( have_rows('partenaire') ): ?>

						<?php while( have_rows('partenaire') ): the_row(); 

							// vars
							$image = get_sub_field('logo');
							?>

							<div class="partenaire">

								<img src="<?php echo $image['sizes']['partenaires_logo']; ?>" alt="<?php echo $image['alt'] ?>" />

							</div>

						<?php endwhile; ?>

					<?php endif; ?>
					

				</div>


					
		
			</div><!--/row-->
			
		</div><!--/container-->
	</div>
	
</div>
<?php get_footer(); ?>