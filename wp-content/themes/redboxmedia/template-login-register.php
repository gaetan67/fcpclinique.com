<?php
/**
 * template name: Login/register
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php 
/*template name: Registor*/
get_header(); ?>


<?php nectar_page_header($post->ID); ?>

<div class="container-wrap">
	
	<div class="container main-content ">

		<?php wc_print_notices(); ?>

		<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

		

		<div class="u-columns col2-set" id="customer_login">

			<div class="u-column1 col-1">

		
				<div class="wrap-login">

					<h2 class="login-title"><?php _e( 'Login', 'woocommerce' ); ?></h2>

					<form method="post" class="login">

						<?php do_action( 'woocommerce_login_form_start' ); ?>
						

						<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
							<label for="username"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" placeholder="<?php echo __('Courriel','redbox'); ?>" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
						</p>
						<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
							<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
							<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" placeholder="<?php echo __('Mot de passe','redbox'); ?>" />
						</p>

						<?php do_action( 'woocommerce_login_form' ); ?>

						<p class="form-row login-btn-row">
							<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
							<input type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Se connecter', 'redbox' ); ?>" /><br/>
							<input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> 
							<label for="rememberme" class="inline"><?php _e( 'Remember me', 'woocommerce' ); ?></label>
						</p>
						<p class="woocommerce-LostPassword lost_password">
							<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
						</p>

						<?php do_action( 'woocommerce_login_form_end' ); ?>

					</form>
				</div>

		<?php //if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

			</div>

			<div class="u-column2 col-2">

				<div class="wrap-register">
						
							<h2><?php _e( 'Abonnement gratuit', 'redbox' ); ?></h2>
							<?php wc_print_notices(); ?>
							<form method="post" class="register">

								<?php do_action( 'woocommerce_register_form_start' ); ?>
								<?php $first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
								$last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';
								$licence = ( ! empty( $_POST['licence'] ) ) ? trim( $_POST['licence'] ) : '';
					        
						        ?>
						        <div class="woocommerce-FormRow--wide form-row form-row-wide if-student">
									<input type="radio" name="student" id="student" value="etudiant" /> <label for="student">Étudiant en pharmacie</label>
									<input type="radio" name="student" id="pharmacien" checked="checked" value="pharmacien" /> <label class="pharmacien" for="pharmacien">Pharmacien</label>
								</div>
						        <p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first">
						            <!--<label for="first_name"><?php _e( 'First Name', 'mydomain' ) ?><br /></label>-->
						                <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="25" placeholder="Prénom*" />
						        </p>
						         <p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last">
						            <!--<label for="last_name"><?php _e( 'Last Name', 'mydomain' ) ?><br /></label>-->
						                <input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="25" placeholder="Nom*" />
						        </p>
						        <div class="clear"></div>
								<?php /*if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

									<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
										<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
										<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
									</p>

								<?php endif;*/ ?>

								<p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first">
									<!--<label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>-->
									<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" placeholder="Courriel*" />
								</p>
								<p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last ">
									<?php
							            $listProvince = array(
							                'Alberta' => "Alberta",
							                "Colombie-Britannique" => "Colombie-Britannique",
							                "Ile-du-Prince-Edouard" => "Ile-du-Prince-Édouard",
							                "Manitoba" => "Manitoba",
							                "Nouveau-Brunswick" => "Nouveau-Brunswick",
							                "Nouvelle-Ecosse" => "Nouvelle-Écosse",
							                "Ontario" => "Ontario",
							                "Quebec" => "Québec",
							                "Saskatchewan" => "Saskatchewan",
							                "Terre-Neuve-et-Labrador" => "Terre-Neuve-et-Labrador",
							                "Nunavut" => "Nunavut",
							                "Territoires du Nord-Ouest" => "Territoires du Nord-Ouest",
							                "Yukon" => "Yukon",
							                "Autre" => "Autre"
							            );
							        ?>
						            <span class="select-style">
								        <select id="user_province" name="user_province" title="SVP sélectionner votre province">
								                <option value="">Votre province*</option>
								            <?php foreach($listProvince as $uneProvince => $provinceValue){ ?>
								                <option value="<?php echo $uneProvince ?>" ><?php echo $provinceValue ?></option>
								            <?php }?>
								            
								            
								        </select>
									</span>
						        </p>
								
						        <div class="clear"></div>
						        <?php
							            $listPratique = array(
							                'pharmacie communautaire' => "pharmacie communautaire",
							                "pharmacie hopital" => "pharmacie d’hôpital",
							                "pharmacien independant" => "pharmacien indépendant",
							                "universite" => "université",
							                "autre" => "autre"
							            );
							        ?>
							        <!--<label for="pratique"></label>-->
							        <p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first not_student">
							        	 <span class="select-style">
									        <select id="pratique" name="pratique" title="SVP sélectionner votre milieu de pratique">
									                <option value="">Milieu de pratique*</option>
									            <?php foreach($listPratique as $unePratique => $pratiqueValue){ ?>
									                <option value="<?php echo $unePratique ?>" ><?php echo $pratiqueValue ?></option>
									            <?php }?>
									            
									            
									        </select>
									    </span>
							        </p>
							        <p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last not_student">
							            <!--<label for="licence"><?php _e( 'Licence', 'mydomain' ) ?><br /></label>-->
							                <input type="text" name="licence" id="licence" class="input" value="<?php echo esc_attr( wp_unslash( $licence ) ); ?>" size="25"  placeholder="Numéro de licence*" />
							        </p>
							        <div class="clear"></div>

								<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

									<p class="woocommerce-FormRow woocommerce-FormRow--first form-row form-row-first">
										<!--<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>-->
										<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" placeholder="Mot de passe*" />
									</p>
									<p class="woocommerce-FormRow woocommerce-FormRow--last form-row form-row-last">
										<!--<label for="conf_password"><?php _e( 'Confirm Password', 'woocommerce' ); ?> <span class="required">*</span></label>-->
										<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="conf_password" id="conf_password" placeholder="Confirmer mot de passe*" />
									</p>
									<div class="clear"></div>

								<?php endif; ?>
									

								<!-- Spam Trap -->
								<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

								<?php do_action( 'woocommerce_register_form' ); ?>
								<?php do_action( 'register_form' ); ?>

								<p class="woocomerce-FormRow form-row thesubmit">
									<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
									<input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( "M'abonner", 'redbox' ); ?>" />
								</p>

								<?php do_action( 'woocommerce_register_form_end' ); ?>

							</form>
						</div>

			</div>
			<div style="clear: both;"></div>

		</div>
		<?php //endif; ?>

		<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
	</div><!--/container-->
	
	</div>
</div>
<?php get_footer(); ?>
